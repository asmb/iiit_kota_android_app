//package in.forsk.iiitk;
//
//import android.content.Context;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import com.squareup.picasso.Picasso;
//
//
//// made by ankit kaushik
//
//public class _2013KUCP1023_GalleryAdapter extends RecyclerView.Adapter<_2013KUCP1023_GalleryAdapter.ViewHolder> {
//
//	 private Context context;
//	    private ItemClickListener1 itemClickListener;
//	    private int groupPosition;
//
//	    _2013KUCP1023_GalleryAdapter(Context c ,int gp){
//	        context = c;
//	        groupPosition = gp;
//	    }
//
//	    public void setItemClickListener(ItemClickListener1 icl ){
//	        itemClickListener = icl;
//	    }
//
//	    @Override
//	    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
//
//	        View rootView = LayoutInflater.from(context).inflate(R.layout._2013kucp1023_gallery_item , viewGroup , false);
//
//	        ViewHolder holder = new ViewHolder(rootView);
//
//	        return holder;
//	    }
//
//	    @Override
//	    public void onBindViewHolder(ViewHolder viewHolder, int i) {
//
//	        _2013KUCP1023_gallery.toolBarHeader.setText(_2013KUCP1023_main_picturegallery.wrapperList.get(groupPosition).getGroupName());
//	        Picasso.with(context).load(_2013KUCP1023_main_picturegallery.wrapperList.get(groupPosition).getImageLink().get(i)).into(viewHolder.wallItem);
//	        //viewHolder.wallItem.setImageResource(R.drawable.wallpaper);
//	    }
//
//	    @Override
//	    public int getItemCount() {
//	        return _2013KUCP1023_main_picturegallery.wrapperList.get(groupPosition).getImageLink().size();
//	    }
//
//	    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
//
//	        ImageView wallItem;
//
//	        public ViewHolder(View itemView) {
//	            super(itemView);
//
//	            wallItem = (ImageView) itemView.findViewById(R.id.galleryImage);
//
//	            wallItem.setOnClickListener(this);
//	        }
//
//	        @Override
//	        public void onClick(View view) {
//	            if (itemClickListener != null){
//	                itemClickListener.onClickListener(view , getAdapterPosition() , groupPosition);
//	            }
//	        }
//	    }
//	}
//
//	interface ItemClickListener1{
//	    void onClickListener(View v , int position , int groupPosition);
//	}