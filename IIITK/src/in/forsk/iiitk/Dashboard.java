package in.forsk.iiitk;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import in.forsk.iiitk.adapter.CustomPagerAdapter;
import in.forsk.iiitk.adapter.NavDrawerListAdapter;
import in.forsk.iiitk.wrapper.NavDrawerItem;

public class Dashboard extends Activity implements
		ViewPager.OnPageChangeListener, OnItemClickListener {
	public final static String TAG = Dashboard.class.getSimpleName();
	private Context context;
	 private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	   

	    private BroadcastReceiver mRegistrationBroadcastReceiver;
	private ActionBar mActionBar;

	// Navigation Drawer
	// http://developer.android.com/reference/android/support/v4/widget/DrawerLayout.html
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	@SuppressWarnings("deprecation")
	private ActionBarDrawerToggle mDrawerToggle;

	// nav drawer title
	private CharSequence mDrawerTitle;

	// used to store app title
	private CharSequence mTitle;

	// slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;

	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;
	ImageView mMenuIcon;

	private ViewPager mBannerViewPager;
	private LinearLayout pager_indicator;
	private int dotsCount;
	private ImageView[] dots;

	/**
	 * Running Activity is visible and interacts with the user. Paused Activity
	 * is still visible but partially obscured, instance is running but might be
	 * killed by the system. Stopped Activity is not visible, instance is
	 * running but might be killed by the system. Killed Activity has been
	 * terminated by the system of by a call to its finish() method.
	 */
	// Change on 26 Aug....

	// Called then the activity is created. Used to initialize the activity, for
	// example create the user interface.
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dashboard);

		context = this;
		GCM();
		// Toast.makeText(context, TAG + " onCreate",
		// //Toast.LENGTH_SHORT).show();
		notify(TAG + " onCreate");

		mActionBar = getActionBar();
		mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

		// Set CustomView in Action bar
		LinearLayout action_bar_view = (LinearLayout) getLayoutInflater()
				.inflate(R.layout.action_bar, null);
		ActionBar.LayoutParams action_params = new ActionBar.LayoutParams(
				ActionBar.LayoutParams.MATCH_PARENT,
				ActionBar.LayoutParams.MATCH_PARENT, Gravity.LEFT);
		mActionBar.setCustomView(R.layout.action_bar);
		mMenuIcon = (ImageView) action_bar_view.findViewById(R.id.menuIcon);
		mMenuIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
					mDrawerLayout.closeDrawer(Gravity.LEFT);
				} else {
					mDrawerLayout.openDrawer(Gravity.LEFT);
				}
			}
		});
		mActionBar.setCustomView(action_bar_view, action_params);

		// load slide menu items
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

		// nav drawer icons from resources
		navMenuIcons = getResources()
				.obtainTypedArray(R.array.nav_drawer_icons);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		// mDrawerLayout.openDrawer(Gravity.LEFT);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

		navDrawerItems = new ArrayList<NavDrawerItem>();

		// adding nav drawer items to array
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons
				.getResourceId(0, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons
				.getResourceId(1, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons
				.getResourceId(2, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons
				.getResourceId(3, -1), true, "22"));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons
				.getResourceId(4, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons
				.getResourceId(5, -1), true, "50+"));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons
				.getResourceId(6, -1)));

		// Recycle the typed array
		navMenuIcons.recycle();

		// setting the nav drawer list adapter
		adapter = new NavDrawerListAdapter(getApplicationContext(),
				navDrawerItems);
		mDrawerList.setAdapter(adapter);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.more_icon, // nav
										// menu
										// toggle
										// icon
				R.string.app_name, // nav drawer open - description for
									// accessibility
				R.string.app_name // nav drawer close - description for
									// accessibility
		) {
			public void onDrawerClosed(View view) {
				// getActionBar().setTitle(mTitle);
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				// calling onPrepareOptionsMenu() to hide action bar icons
				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		mDrawerList.setOnItemClickListener(this);

		/**
		 * Dashbord banner pager linking
		 */
		mBannerViewPager = (ViewPager) findViewById(R.id.pager);
		pager_indicator = (LinearLayout) findViewById(R.id.viewPagerCountDots);
		mBannerViewPager.getLayoutParams().height = (int) (Application.mDeviceHeight * .50);

		CustomPagerAdapter customAdapter = new CustomPagerAdapter(context,
				getResources().obtainTypedArray(R.array.dashboard_banner));
		mBannerViewPager.setAdapter(customAdapter);
		mBannerViewPager.setOnPageChangeListener(this);

		dotsCount = customAdapter.getCount();
		dots = new ImageView[dotsCount];

		for (int i = 0; i < dotsCount; i++) {
			dots[i] = new ImageView(this);
			dots[i].setImageDrawable(getResources().getDrawable(
					R.drawable.nonselecteditem_dot));

			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.WRAP_CONTENT,
					LinearLayout.LayoutParams.WRAP_CONTENT);

			params.setMargins(6, 0, 6, 0);

			pager_indicator.addView(dots[i], params);
		}

		dots[0].setImageDrawable(getResources().getDrawable(
				R.drawable.selecteditem_dot));

	}

	@Override
	public void onPageScrolled(int position, float positionOffset,
			int positionOffsetPixels) {

	}

	@Override
	public void onPageSelected(int position) {
		for (int i = 0; i < dotsCount; i++) {
			dots[i].setImageDrawable(getResources().getDrawable(
					R.drawable.nonselecteditem_dot));
		}

		dots[position].setImageDrawable(getResources().getDrawable(
				R.drawable.selecteditem_dot));
	}

	@Override
	public void onPageScrollStateChanged(int state) {

	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}
	 @Override
	    protected void onResume() {
	        super.onResume();
	        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
	                new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
	    }

	    @Override
	    protected void onPause() {
	        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
	        super.onPause();
	    }
	/**
	 * Click Listener for dashboard option
	 */
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.aboutUs:
			// Click Event for About us
			// Toast.makeText(context, "About Us", //Toast.LENGTH_SHORT).show();
//			startActivity(new Intent(context, FacultyProfile.class));
			break;
		case R.id.notification:
			// Click Event for Notification
			// Toast.makeText(context, "Notification",
			// //Toast.LENGTH_SHORT).show();
			startActivity(new Intent(context, LoginScreen.class));
			break;
		case R.id.homework:
			// Click Event for Home work
			// Toast.makeText(context, "Home work",
			// //Toast.LENGTH_SHORT).show();
			startActivity(new Intent(context, Joey_2013kucp1021TimeTable.class));
			break;
		case R.id.attendance:
			// Click Event for Attendance
			// Toast.makeText(context, "Attendance",
			// //Toast.LENGTH_SHORT).show();
			break;
		case R.id.calendar:
			// Click Event for Calendar
			// Toast.makeText(context, "Calendar", //Toast.LENGTH_SHORT).show();
//			startActivity(new Intent(context, Anita_2013kucpanita_1020NewsList.class));
			break;
		case R.id.contactus:
			// Click Event for Contact Us
			// Toast.makeText(context, "Contact Us",
			// //Toast.LENGTH_SHORT).show();
			break;
		default:
			break;
		}
	}

	/**
	 * Click Listener for navigation drawer list
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		switch (position) {
		case 0:
			// Click Event for Academics
			// Toast.makeText(context, "Academics",
			// //Toast.LENGTH_SHORT).show();
			break;
		case 1:
			// Click Event for Admission
			// Toast.makeText(context, "Admission",
			// //Toast.LENGTH_SHORT).show();
			break;
		case 2:
			// Click Event for Library
			// Toast.makeText(context, "Library", //Toast.LENGTH_SHORT).show();
			break;
		case 3:
			// Click Event for Campus Tour
			// Toast.makeText(context, "Campus Tour",
			// //Toast.LENGTH_SHORT).show();
			break;
		case 4:
			// Click Event for Direction
			// Toast.makeText(context, "Direction",
			// //Toast.LENGTH_SHORT).show();
			break;
		case 5:
			// Click Event for Share
			// Toast.makeText(context, "Share", //Toast.LENGTH_SHORT).show();
			break;
		default:
			break;
		}
	}

	@Override
	public void onBackPressed() {
		if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
			mDrawerLayout.closeDrawer(Gravity.LEFT);
		} else {
			super.onBackPressed();
		}

	}

	@Override
	protected void onStart() {
		super.onStart();

		// Toast.makeText(context, TAG + " onStart",
		// //Toast.LENGTH_SHORT).show();
		notify(TAG + " onStart");
	}

	@Override
	protected void onRestart() {
		super.onRestart();

		// Toast.makeText(context, TAG + " onRestart",
		// //Toast.LENGTH_SHORT).show();
		notify(TAG + " onRestart");
	}

	// Called if the activity get visible again and the user starts interacting
	// with the activity again. Used to initialize fields, register listeners,
	// bind to services, etc.
	

	// Called once another activity gets into the foreground. Always called
	// before the activity is not visible anymore. Used to release resources or
	// save application data. For example you unregister listeners, intent
	// receivers, unbind from services or remove system service listeners.
	

	// Called once the activity is no longer visible. Time or CPU intensive
	// shut-down operations, such as writing information to a database should be
	// down in the onStop() method. This method is guaranteed to be called as of
	// API 11.
	

	@SuppressLint("NewApi")
	private void notify(String methodName) {
		// String name = this.getClass().getName();
		// String[] strings = name.split("\\.");
		// Notification noti = new
		// Notification.Builder(this).setContentTitle(methodName + " " +
		// strings[strings.length -
		// 1]).setAutoCancel(true).setSmallIcon(R.drawable.ic_launcher)
		// .setContentText(name).build();
		// Comment by sorbh
		// NotificationManager notificationManager = (NotificationManager)
		// getSystemService(NOTIFICATION_SERVICE);
		// notificationManager.notify((int) System.currentTimeMillis(), noti);
	}

   
    

   
    protected void GCM(){

       mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
                	Toast.makeText(context, "Registered",Toast.LENGTH_LONG).show();
                } else {
                	Toast.makeText(context, "Error",Toast.LENGTH_LONG).show();
                }
            }
        };
        

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
//            Intent intent = new Intent(this, RegistrationIntentService.class);
//            startService(intent);
//            System.out.println("reached");
        }
        else
        {
        	System.out.println("reached2");
        }
    }

   
   
    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
//        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
//        
//        // Showing status 
//        if(status==ConnectionResult.SUCCESS)
//            return true;
//        else{ 
//            
//            int requestCode = 10;
//            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
//            dialog.show();
//            return false;
//        }
    	
    	return false;
    }

}
