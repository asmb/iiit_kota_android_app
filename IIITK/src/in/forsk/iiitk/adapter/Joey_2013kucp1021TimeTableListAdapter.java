package in.forsk.iiitk.adapter;

import in.forsk.iiitk.R;
import in.forsk.iiitk.wrapper.FacultyWrapper;
import in.forsk.iiitk.wrapper.Joey_2013kucp1021TimeTableWrapper;

import java.util.ArrayList;

import com.androidquery.AQuery;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class Joey_2013kucp1021TimeTableListAdapter extends BaseAdapter {
	private final static String TAG = Joey_2013kucp1021TimeTableListAdapter.class.getSimpleName();
	Context context;
	ArrayList<Joey_2013kucp1021TimeTableWrapper> mTimeTableDataList;
	LayoutInflater inflater;
	ViewHoder holder;


	public Joey_2013kucp1021TimeTableListAdapter(Context context, ArrayList<Joey_2013kucp1021TimeTableWrapper> mTimeTableDataList) {
		this.context = context;
		this.mTimeTableDataList = mTimeTableDataList;

		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		
	}

	@Override
	public int getCount() {
		return mTimeTableDataList.size();
	}

	@Override
	public Object getItem(int position) {
		return mTimeTableDataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.joey_2013kucp1021_row_time_table, null);

			holder = new ViewHoder(convertView);

			convertView.setTag(holder);
		} else {

			// this is called when you flick the list to see the other item.
			// (Or can say whem getView method reuse the view)
			holder = (ViewHoder) convertView.getTag();
		}

		Joey_2013kucp1021TimeTableWrapper obj = mTimeTableDataList.get(position);

		
		holder.date.setText(obj.getDate());
		holder.time.setText(obj.getTime());
		holder.subject.setText(obj.getSubject());
		holder.faculty.setText(obj.getFaculty());

		return convertView;
	}

	public static class ViewHoder {
		TextView date, time, subject,faculty;

		public ViewHoder(View view) {

			date = (TextView) view.findViewById(R.id.date);
			time = (TextView) view.findViewById(R.id.time);
			subject = (TextView) view.findViewById(R.id.subject);
			faculty = (TextView) view.findViewById(R.id.faculty);
		}
	}

}
