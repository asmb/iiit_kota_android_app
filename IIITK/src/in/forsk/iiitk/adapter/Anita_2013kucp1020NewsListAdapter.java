package in.forsk.iiitk.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import in.forsk.iiitk.R;
import in.forsk.iiitk.wrapper.Anita_2013kucp1020NewsListWrapper;

public class Anita_2013kucp1020NewsListAdapter extends BaseAdapter {
	private final static String TAG = Anita_2013kucp1020NewsListAdapter.class.getSimpleName();
	Context context;
	ArrayList<Anita_2013kucp1020NewsListWrapper> mNewsListDataList;
	LayoutInflater inflater;
	ViewHoder holder;

	

	public Anita_2013kucp1020NewsListAdapter(Context context, ArrayList<Anita_2013kucp1020NewsListWrapper> mNewsListDataList) {
		this.context = context;
		this.mNewsListDataList = mNewsListDataList;

		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		
	}

	@Override
	public int getCount() {
		return mNewsListDataList.size();
	}

	@Override
	public Object getItem(int position) {
		return mNewsListDataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.a_2013kucp1020_activity_news_list, null);

			holder = new ViewHoder(convertView);

			convertView.setTag(holder);
		} else {

			// this is called when you flick the list to see the other item.
			// (Or can say whem getView method reuse the view)
			holder = (ViewHoder) convertView.getTag();
		}

		Anita_2013kucp1020NewsListWrapper obj = mNewsListDataList.get(position);

		System.out.println(obj.getDate());
		if (obj.getDate().equals("")) {
			holder.header.setVisibility(View.GONE);
			System.out.println("hidden");
		}
		 else 		{
			 holder.header.setVisibility(View.VISIBLE);
			 holder.header.setText(obj.getDate());
			 System.out.println("displayed");
		 }
		
		holder.title.setText(obj.getTitle());
		holder.subtitle.setText(obj.getSubtitle());
		

		return convertView;
	}

	public static class ViewHoder {
		TextView header, title, subtitle;

		public ViewHoder(View view) {

			header = (TextView) view.findViewById(R.id.newslistheader);
			title = (TextView) view.findViewById(R.id.newslisttitle);
			subtitle = (TextView) view.findViewById(R.id.newslistsubtitile);
			
		}
	}

}
