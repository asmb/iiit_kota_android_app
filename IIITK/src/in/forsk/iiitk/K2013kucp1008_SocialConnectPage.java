package in.forsk.iiitk;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class K2013kucp1008_SocialConnectPage extends Activity {

	WebView webview;
	String url = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_k2013kucp1008__social_connect_page);
		webview = (WebView) findViewById(R.id.webView);
		Intent i = getIntent();
		url = i.getStringExtra("url");// get the url sent from previous activity
		webview.setWebViewClient(new MyWebClient());
		webview.getSettings().setJavaScriptEnabled(true);
		webview.loadUrl(url);
	}

	public void onClickBack(View v) {
		finish();
	}

	public class MyWebClient extends WebViewClient {
		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			// TODO Auto-generated method stub
			super.onPageStarted(view, url, favicon);
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			// TODO Auto-generated method stub

			view.loadUrl(url);
			return true;

		}
	}

	// To handle "Back" key press event for WebView to go back to previous
	// screen.
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK) && webview.canGoBack()) {
			webview.goBack();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
