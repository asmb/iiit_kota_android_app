package in.forsk.iiitk;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class K_2013KUCP1029_PreferenceManager {

	private static K_2013KUCP1029_PreferenceManager instance;
	private SharedPreferences mPref;
	private Editor editor;

	private final static String PREF_NAME = "facebook_pref";
	private final static int PRIVATE_MODE = 0;

	public static K_2013KUCP1029_PreferenceManager getInstance(Context context) {
		if (instance == null)
			instance = new K_2013KUCP1029_PreferenceManager(context);
		return instance;
	}

	private K_2013KUCP1029_PreferenceManager(Context context) {
		mPref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = mPref.edit();
	}

	private final static String ACCESS_TOKEN = "access_token_key";

	public void setAcessToken(String s) {
		editor.putString(ACCESS_TOKEN, s);
		editor.commit();
	}

	public String getAccessToken() {
		return mPref.getString(ACCESS_TOKEN, "");
	}

}
