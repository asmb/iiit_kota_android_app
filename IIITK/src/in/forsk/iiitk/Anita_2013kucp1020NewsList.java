//package in.forsk.iiitk;
//
//import in.forsk.iiitk.adapter.Anita_2013kucpanita_1020NewsListAdapter;
//import in.forsk.iiitk.wrapper.Anita_2013kucpanita_1020NewsListWrapper;
//import twitter4j.internal.org.json.JSONArray;
//import twitter4j.internal.org.json.JSONObject;
//
//public class Anita_2013kucpanita_1020NewsList extends Activity {
//
//	
//	ListView news;
//	private Context context;
//	Anita_2013kucpanita_1020NewsListAdapter mAdapter;
//	ArrayList<Anita_2013kucpanita_1020NewsListWrapper> mNewsListDataList= new ArrayList<Anita_2013kucpanita_1020NewsListWrapper>();
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_news_list);
//		context = this;
//		news=(ListView) findViewById(R.id.NewsListView);
//		
//		 parseNewsList("http://192.168.229.1/iiitk/android/news.php");
//		 
//		 news.setOnItemClickListener(new OnItemClickListener() { 
//	            
//
//				@Override
//				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//					// TODO Auto-generated method stub
//					
//					
//					Anita_2013kucpanita_1020_NewsDetail.Id=mNewsListDataList.get(position).getId();
//					
//					startActivity(new Intent(context, Anita_2013kucpanita_1020_NewsDetail.class));
//				} 
//	        });
//		
//	}
//	private InputStream openHttpConnection(String urlStr) {
//		InputStream in = null;
//		int resCode = -1;
//
//		try {
//			URL url = new URL(urlStr);
//			URLConnection urlConn = url.openConnection();
//
//			if (!(urlConn instanceof HttpURLConnection)) {
//				throw new IOException("URL is not an Http URL");
//			}
//
//			HttpURLConnection httpConn = (HttpURLConnection) urlConn;
//			httpConn.setAllowUserInteraction(false);
//			httpConn.setInstanceFollowRedirects(true);
//			httpConn.setRequestMethod("GET");
//			
//			httpConn.connect();
//
//			resCode = httpConn.getResponseCode();
//			if (resCode == HttpURLConnection.HTTP_OK) {
//				in = httpConn.getInputStream();
//			}
//		} catch (MalformedURLException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return in;
//	}
//	class NewsListParseAsyncTask extends AsyncTask<String, Integer, String> {
//      
//		ProgressDialog pd;
//
//		@Override
//		protected void onPreExecute() {
//			super.onPreExecute();
//
//			pd = new ProgressDialog(context);
//			pd.setMessage("loading..");
//			pd.setCancelable(false);
//			pd.show();
//		}
//
//		@Override
//		protected String doInBackground(String... params) {
//			String url = params[0];
//			InputStream is = openHttpConnection(url);
//			String response = "";
//			try {
//				response = convertStreamToString(is);
//
//				onProgressUpdate(0);
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//			return response;
//		}
//
//		@Override
//		protected void onProgressUpdate(Integer... values) {
//			// TODO Auto-generated method stub
//			super.onProgressUpdate(values);
//		}
//
//		@Override
//		protected void onPostExecute(String result) {
//			super.onPostExecute(result);
//			
//			 mNewsListDataList = parseLocalFile(result);
//			
//			setNewsListAdapter(mNewsListDataList);
//
//			if (pd != null)
//				pd.dismiss();
//		}
//	}
//	public ArrayList<Anita_2013kucpanita_1020NewsListWrapper> parseLocalFile(String json_string) {
//
//		ArrayList<Anita_2013kucpanita_1020NewsListWrapper> mNewsListDataList = new ArrayList<Anita_2013kucpanita_1020NewsListWrapper>();
//		try {
//			System.out.println(json_string);
//			// Converting multipal json data (String) into Json array
//			JSONArray newslistArray = new JSONArray(json_string);
//			Log.d("joey", newslistArray.toString());
//			// Iterating json array into json objects
//			ArrayList<Anita_2013kucpanita_1020NewsListWrapper> days = new ArrayList<Anita_2013kucpanita_1020NewsListWrapper>();
//			int today=0,yesterday=0;
//			for (int i = 0; newslistArray.length() > i; i++) {
//
//				// Extracting json object from particular index of array
//				JSONObject newslistJsonObject = newslistArray.getJSONObject(i);
//               
//			//	if(newslistJsonObject.get("Date").equals(""))newslistJsonObject.put("Date","Today");
//				// Design patterns
//				Anita_2013kucpanita_1020NewsListWrapper newslistObject = new Anita_2013kucpanita_1020NewsListWrapper(newslistJsonObject);
//
//				SimpleDateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH);
//			    String cDateTime=dateFormat.format(new Date());
//			    SimpleDateFormat  format = new SimpleDateFormat("yyyy-MM-dd");  
//                  
//                    Date date = format.parse(cDateTime); 
//                    Calendar c = Calendar.getInstance();
//                    c.setTime(date);
// 
//                    c.add(Calendar.DATE, -1);
//                 String   prevday = format.format(c.getTime());
//                 System.out.println(cDateTime+prevday);
//                 System.out.println(newslistObject.getDate());
//				if(newslistObject.getDate().equals(cDateTime)&&today==0){newslistObject.setDate("Today");today=1;}
//				else if(newslistObject.getDate().equals(cDateTime)&&today==1){newslistObject.setDate("");}
//				else if(newslistObject.getDate().equals(prevday)&&yesterday==0){newslistObject.setDate("Yesterday");yesterday=1;}
//				else if(newslistObject.getDate().equals(prevday)&&yesterday==1){newslistObject.setDate("");}
//				
//				mNewsListDataList.add(newslistObject);
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return mNewsListDataList;
//	}
//	private String convertStreamToString(InputStream is) throws IOException {
//		// Converting input stream into string
//		ByteArrayOutputStream baos = new ByteArrayOutputStream();
//		int i = is.read();
//		while (i != -1) {
//			baos.write(i);
//			i = is.read();
//		}
//		return baos.toString();
//	}
//	private void parseNewsList(final String url) {
//		// TODO Auto-generated method stub
//		NewsListParseAsyncTask tt=new NewsListParseAsyncTask();
//		
//		tt.execute(url);
//		
//		
//	}
//
//	public void setNewsListAdapter(ArrayList<Anita_2013kucpanita_1020NewsListWrapper> DataList) {
//		
//		mAdapter = new Anita_2013kucpanita_1020NewsListAdapter(context,DataList);
//		
//		 news.setAdapter(mAdapter);
//		
//	}
//	
//	
//}
//>>>>>>> remotes/pintojoey/iiit_kota_android_app/master
