package in.forsk.iiitk;

import java.util.List;

public class _2013KUCP1017_PictureGallery_ImageWrapper {


    String groupName;
    String groupImageLink;
    List<String> imageLink;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }



    public List<String> getImageLink() {
        return imageLink;
    }

    public void setImageLink(List<String> imageLink) {
        this.imageLink = imageLink;
    }


    public String getGroupImageLink() {
        return groupImageLink;
    }

    public void setGroupImageLink(String groupImageLink) {
        this.groupImageLink = groupImageLink;
    }


}
