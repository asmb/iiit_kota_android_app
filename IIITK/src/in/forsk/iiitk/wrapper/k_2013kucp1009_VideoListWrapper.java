package in.forsk.iiitk.wrapper;

import java.util.ArrayList;

import org.json.JSONObject;

//In general, a wrapper class is any class which "wraps" or "encapsulates" the functionality of another class
//or component. These are useful by providing a level of abstraction from the 
//implementation of the underlying class or component
//http://www.javawithus.com/tutorial/wrapper-classes
public class k_2013kucp1009_VideoListWrapper {

	private String video_url = "";
	private String thumbnail = "";
	private String  video_title = "";
	private String  duration = "";
	private String  description = "";
	
	public k_2013kucp1009_VideoListWrapper() {
		// TODO Auto-generated constructor stub
	}

	public k_2013kucp1009_VideoListWrapper(JSONObject jObj) {
		try {

			System.out.println(jObj.toString());

			//http://online.mnit.ac.in/iiitk/images/disp.png
			//video_url ="http://online.mnit.ac.in/iiitk/images/"+jObj.getString("video_url");
			
			video_url =jObj.getString("video_url");
			
			thumbnail ="http://online.mnit.ac.in/iiitk/images/"+jObj.getString("thumbnail");
			//thumbnail ="http://10.0.2.2/android/"+jObj.getString("thumbnail");
			video_title = jObj.getString("video_title");
			duration = jObj.getString("duration");
			description = jObj.getString("description");
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	
	
	public String getvideo_url() {
		return video_url;
	}

	public void setvideo_url(String video_url) {
		this.video_url = video_url;
	}

	
	
	public String getthumbnail() {
		return thumbnail;
	}

	public void setthumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	
	public String getvideo_title() {
		return video_title;
	}

	public void setvideo_title(String video_title) {
		this.video_title = video_title;
	}

	public String getduration() {
		return duration;
	}

	public void setduration(String duration) {
		this.duration = duration;
	}

	
	public String getdescription() {
		return description;
	}

	public void setdescription(String description) {
		this.description = description;
	}

	
	

}
