package in.forsk.iiitk.wrapper;

import java.util.ArrayList;

import org.json.JSONObject;

//In general, a wrapper class is any class which "wraps" or "encapsulates" the functionality of another class
//or component. These are useful by providing a level of abstraction from the 
//implementation of the underlying class or component
//http://www.javawithus.com/tutorial/wrapper-classes
public class k_2013kucp1009_VideoWrapper {

	private String title = "";
	private String t_video_no = "";
	private String image_url = "";
	
	public k_2013kucp1009_VideoWrapper() {
		// TODO Auto-generated constructor stub
	}

	public k_2013kucp1009_VideoWrapper(JSONObject jObj) {
		try {

			System.out.println(jObj.toString());

			title = jObj.getString("title");
			t_video_no = jObj.getString("t_video_no");
			image_url ="http://online.mnit.ac.in/iiitk/images/"+jObj.getString("image_url");
			//image_url ="http://10.0.2.2/android/"+jObj.getString("image_url");
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String first_name) {
		this.title = first_name;
	}

	public String getVideo_no() {
		return t_video_no;
	}

	public void setVideo_no(String last_name) {
		this.t_video_no = last_name;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String photo) {
		this.image_url = photo;
	}


}
