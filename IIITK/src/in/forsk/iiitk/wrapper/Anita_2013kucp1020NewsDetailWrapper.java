package in.forsk.iiitk.wrapper;

import java.util.ArrayList;

import org.json.JSONObject;

//In general, a wrapper class is any class which "wraps" or "encapsulates" the functionality of another class
//or component. These are useful by providing a level of abstraction from the 
//implementation of the underlying class or component
//http://www.javawithus.com/tutorial/wrapper-classes
public class Anita_2013kucp1020NewsDetailWrapper {

	private String date = "";
	private String title = "";
	private String subtitle = "";
	private String detail = "";
	private String image = "";
	
	

	public Anita_2013kucp1020NewsDetailWrapper() {
		// TODO Auto-generated constructor stub
	}

	public Anita_2013kucp1020NewsDetailWrapper(JSONObject jObj) {
		try {

			System.out.println(jObj.toString());

			setDate(jObj.getString("Date"));
			setTitle(jObj.getString("Title"));
			setSubtitle(jObj.getString("Subtitle"));
			setImage(jObj.getString("Image"));
			setDetail(jObj.getString("Detail"));
			
			

			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	
	

}
