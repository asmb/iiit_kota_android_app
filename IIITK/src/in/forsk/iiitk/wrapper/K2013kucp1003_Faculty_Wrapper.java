package in.forsk.iiitk.wrapper;

import org.json.JSONObject;

public class K2013kucp1003_Faculty_Wrapper {
	private static String name = "";
	private static String designation="";
	private static String photo = "";
	private static String department = "";
	private static String reserch_area = "";
	private static String hometown="";
	private static String faculty_id="";
	private static String phone = "";
	private static String email = "";
	private static String qualifications = "";
	private static String facebook = "";
	private static String achievements = "";
	

	public K2013kucp1003_Faculty_Wrapper() {
		// TODO Auto-generated constructor stub
	}

	public K2013kucp1003_Faculty_Wrapper(JSONObject jObj) {
		try {

			System.out.println(jObj.toString());

			name = jObj.getString("Name");
			
			photo = jObj.getString("Image");
			department = jObj.getString("Department");
			reserch_area = jObj.getString("ResearchAreas");

			phone = jObj.getString("Contact");
			email = jObj.getString("Email");
            facebook = jObj.getString("Facebook");
			hometown = jObj.getString("Hometown");
			achievements = jObj.getString("Achievements");
			qualifications = jObj.getString("Qualification");
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		K2013kucp1003_Faculty_Wrapper.name = name;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		K2013kucp1003_Faculty_Wrapper.designation = designation;
	}

	public String getCity() {
		return hometown;
	}

	public void setCity(String city) {
		K2013kucp1003_Faculty_Wrapper.hometown = city;
	}

	public String getFaculty_id() {
		return faculty_id;
	}

	public void setFaculty_id(String faculty_id) {
		K2013kucp1003_Faculty_Wrapper.faculty_id = faculty_id;
	}

	public String getQualifications() {
		return qualifications;
	}

	public void setQualifications(String qualifications) {
		K2013kucp1003_Faculty_Wrapper.qualifications = qualifications;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		K2013kucp1003_Faculty_Wrapper.facebook = facebook;
	}

	public String getAchievements() {
		return achievements;
	}

	public void setAchievements(String achievements) {
		K2013kucp1003_Faculty_Wrapper.achievements = achievements;
	}

	public String getname() {
		return name;
	}

	public void setname(String name) {
		K2013kucp1003_Faculty_Wrapper.name = name;
	}

	
	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		K2013kucp1003_Faculty_Wrapper.photo = photo;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		K2013kucp1003_Faculty_Wrapper.department = department;
	}

	public String getReserch_area() {
		return reserch_area;
	}

	public void setReserch_area(String reserch_area) {
		K2013kucp1003_Faculty_Wrapper.reserch_area = reserch_area;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		K2013kucp1003_Faculty_Wrapper.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		K2013kucp1003_Faculty_Wrapper.email = email;
	}

	


}
