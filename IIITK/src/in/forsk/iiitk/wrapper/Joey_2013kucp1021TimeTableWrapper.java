package in.forsk.iiitk.wrapper;

import java.util.ArrayList;

import org.json.JSONObject;

//In general, a wrapper class is any class which "wraps" or "encapsulates" the functionality of another class
//or component. These are useful by providing a level of abstraction from the 
//implementation of the underlying class or component
//http://www.javawithus.com/tutorial/wrapper-classes
public class Joey_2013kucp1021TimeTableWrapper {

	private String date = "";
	private String time = "";
	private String subject = "";
	private String faculty = "";
	private String sem = "";
	

	public Joey_2013kucp1021TimeTableWrapper() {
		// TODO Auto-generated constructor stub
	}

	public Joey_2013kucp1021TimeTableWrapper(JSONObject jObj) {
		try {

			System.out.println(jObj.toString());

			setDate(jObj.getString("Date"));
			setTime(jObj.getString("Time"));
			setSubject(jObj.getString("Subject"));
			setFaculty(jObj.getString("Faculty"));
			

			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getFaculty() {
		return faculty;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}

	public String getSem() {
		return sem;
	}

	public void setSem(String sem) {
		this.sem = sem;
	}

	

}
