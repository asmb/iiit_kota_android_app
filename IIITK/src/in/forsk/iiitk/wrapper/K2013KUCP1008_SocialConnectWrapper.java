package in.forsk.iiitk.wrapper;

import org.json.JSONObject;

public class K2013KUCP1008_SocialConnectWrapper {

	String connectId="";
	String connectUrl="";
	String openInApp="";
	
	public String getOpenInApp() {
		return openInApp;
	}
	public void setOpenInApp(String openInApp) {
		this.openInApp = openInApp;
	}
	public String getConnectId() {
		return connectId;
	}
	public void setConnectId(String connectId) {
		this.connectId = connectId;
	}
	
	public String getConnectUrl() {
		return connectUrl;
	}
	public void setConnectUrl(String connectUrl) {
		this.connectUrl = connectUrl;
	}
	
	public K2013KUCP1008_SocialConnectWrapper(JSONObject jObj) {
		try {

			System.out.println(jObj.toString());

			connectId = jObj.getString("connectId");
			connectUrl = jObj.getString("connectUrl");
			openInApp=jObj.getString("openinapp");

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
	}
}
