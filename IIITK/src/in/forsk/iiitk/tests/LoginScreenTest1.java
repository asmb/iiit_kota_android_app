package in.forsk.iiitk.tests;

import in.forsk.iiitk.LoginScreen;
import in.forsk.iiitk.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.EditText;

public class LoginScreenTest1 extends ActivityInstrumentationTestCase2<LoginScreen> {

	EditText nameEt, emailEt, passEt;
	Button loginBtn;

	public LoginScreenTest1() {
		super(LoginScreen.class);
	}

	@Before
	protected void setUp() throws Exception {
		super.setUp();

		nameEt = (EditText) getActivity().findViewById(R.id.nameEt);
		emailEt = (EditText) getActivity().findViewById(R.id.emailEt);
		passEt = (EditText) getActivity().findViewById(R.id.passEt);

		loginBtn = (Button) getActivity().findViewById(R.id.loginBtn);
	}

	@Test
	public void testLogin() {

		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				nameEt.setText("Saurabh");
				emailEt.setText("saurabh@gmail.com");
				passEt.setText("1234");

				loginBtn.performClick();
			}
		});

		// Stop the activity - The onDestroy() method should save the state of
		// the Spinner
		getActivity().finish();

		// Re-start the Activity - the onResume() method should restore the
		// state of the Spinner
		getActivity();

		String name = nameEt.getText().toString();

		assertEquals("Test Successful", "Saurabh1", name);

	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

}
