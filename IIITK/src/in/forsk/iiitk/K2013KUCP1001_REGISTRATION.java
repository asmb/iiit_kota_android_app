package in.forsk.iiitk;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

//Registration module 2013kucp1001,2013kucp1005




import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;


import android.text.Editable;
import android.util.Log;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class K2013KUCP1001_REGISTRATION extends Activity {
String Email,Result;
String Vercode;
Context context;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_k2013_kucp1001__registration);
		context=this;
		   ImageButton Verify=(ImageButton) findViewById(R.id.EmailButton);
	Verify.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			EditText Email_Field=(EditText) findViewById(R.id.Email);
	        Email= Email_Field.getText().toString();
          String link="http://192.168.229.1/iiitk/assets/php/smsverify.php?email="+Email;
        Log.d("http",link);
	        new EmailVerifyTask().execute(link);
	
		}
	});  
	
	ImageButton Check=(ImageButton)findViewById(R.id.Check);
	Check.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			EditText Vercode_Field=(EditText) findViewById(R.id.vercode);
	        Vercode=  Vercode_Field.getText().toString();
	        String link="http://192.168.229.1/iiitk/assets/php/smscode.php?email="+Email+"&vercode="+Vercode;
	        System.out.println(link);
	        new EmailCheckTask().execute(link);
	        Log.d("http",link);
if(Result.equals("success")) Toast.makeText(context, "Verified", Toast.LENGTH_SHORT);
else Toast.makeText(context, "Incorrect", Toast.LENGTH_SHORT);
		}
	});
		}

	

	
	



//Params, Progress, Result
class EmailVerifyTask extends AsyncTask<String, Integer, String> {

	ProgressDialog pd;

	@Override
	protected void onPreExecute() {
		super.onPreExecute();

		pd = new ProgressDialog(context);
		pd.setMessage("loading..");
		pd.setCancelable(false);
		pd.show();
	}

	@Override
	protected String doInBackground(String... params) {
		String url = params[0];
		
		String response ="";
		try {
			response = openHttpConnection(url);

			onProgressUpdate(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	
	@Override
	protected void onProgressUpdate(Integer... values) {
		// TODO Auto-generated method stub
		super.onProgressUpdate(values);
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		Toast.makeText(context, "Check Your Email", Toast.LENGTH_SHORT).show();

		if (pd != null)
			pd.dismiss();
	}
}
class EmailCheckTask extends AsyncTask<String, Integer, String> {
	String response;
	ProgressDialog pd;

	@Override
	protected void onPreExecute() {
		super.onPreExecute();

		pd = new ProgressDialog(context);
		pd.setMessage("loading..");
		pd.setCancelable(false);
		pd.show();
	}

	@Override
	protected String doInBackground(String... params) {
		String url = params[0];
		
		 response ="";
		try {
			response = openHttpConnection(url);

			onProgressUpdate(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	
	@Override
	protected void onProgressUpdate(Integer... values) {
		// TODO Auto-generated method stub
		super.onProgressUpdate(values);
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		Result=response;
		Toast.makeText(context, "Check Your Email", Toast.LENGTH_SHORT).show();

		if (pd != null)
			pd.dismiss();
	}
}
private String convertStreamToString(InputStream is) throws IOException {
	// Converting input stream into string
	ByteArrayOutputStream baos = new ByteArrayOutputStream();
	int i = is.read();
	while (i != -1) {
		baos.write(i);
		i = is.read();
	}
	return baos.toString();
}
	private String openHttpConnection(String urlStr) throws IOException {
		InputStream in = null;
		int resCode = -1;

		try {
			URL url = new URL(urlStr);
			URLConnection urlConn = url.openConnection();

			if (!(urlConn instanceof HttpURLConnection)) {
				throw new IOException("URL is not an Http URL");
			}

			HttpURLConnection httpConn = (HttpURLConnection) urlConn;
			httpConn.setAllowUserInteraction(false);
			httpConn.setInstanceFollowRedirects(true);
			httpConn.setRequestMethod("GET");
			httpConn.connect();

			resCode = httpConn.getResponseCode();
			if (resCode == HttpURLConnection.HTTP_OK) {
				in = httpConn.getInputStream();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.Result=convertStreamToString(in);
		return convertStreamToString(in);
	}

}

